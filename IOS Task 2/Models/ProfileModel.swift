import Foundation
import SwiftyJSON
// MARK: - TranformerModel
// MARK: - Transformer
class TranformerModel {
    let courage, endurance, firepower: Int
    let id: String
    let intelligence: Int
    let name: String
    let rank, skill, speed, strength: Int
    let team: String
    let teamIcon: String

    init(json:JSON) {
        self.courage         = json["courage"].intValue
        self.endurance       = json["endurance"].intValue
        self.firepower       = json["firepower"].intValue
        self.id              = json["id"].stringValue
        self.intelligence    = json["intelligence"].intValue
        self.name            = json["name"].stringValue
        self.rank            = json["rank"].intValue
        self.skill           = json["skill"].intValue
        self.speed           = json["speed"].intValue
        self.strength        = json["strength"].intValue
        self.team            = json["team"].stringValue
        self.teamIcon        = json["teamIcon"].stringValue
    }
}
