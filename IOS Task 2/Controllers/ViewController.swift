//
//  ViewController.swift
//  IOS Task 2
//
//  Created by Macbook on 4/2/21.
//

import UIKit
import Alamofire
import Kingfisher
class ViewController: UIViewController {
    
    
    @IBOutlet weak var TV_AutoBots:UITableView!
    @IBOutlet weak var TV_Deception:UITableView!
    var selectedAutobotsID = String()
    var selectedDecpticonID = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.CallingApi()
    }

    func CallingApi()
    {
        
        TV_Deception.register(UINib(nibName: "TranformersCells_TVC", bundle: nil), forCellReuseIdentifier: "TranformersCells_TVC")
        TV_AutoBots.register(UINib(nibName: "TranformersCells_TVC", bundle: nil), forCellReuseIdentifier: "TranformersCells_TVC")
        TranformerDataModel.shareInstance.getDataFromApi(complete: { responseString in
            self.TV_AutoBots.dataSource = self
            self.TV_AutoBots.delegate = self
            self.TV_Deception.delegate = self
            self.TV_Deception.dataSource = self
            self.TV_Deception.reloadData()
            self.TV_AutoBots.reloadData()
        })
    }
    
    @IBAction func Action_Fight(_sender:UIBarButtonItem)
    {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "Fight_VC") as! Fight_VC
        newViewController.selectedAutobotsID =  self.selectedAutobotsID
        newViewController.selectedDecpticonID = self.selectedDecpticonID
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
}


extension ViewController:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == TV_AutoBots
        {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "TranformersCells_TVC", for: indexPath)  as? TranformersCells_TVC  else {
                return UITableViewCell()
            }
            cell.lblName.text = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].name
            cell.lblTeamname.text = "\("TeamName:") \(TranformerDataModel.shareInstance.autoBotsData[indexPath.row].team)"
            cell.ivProfile.kf.setImage(with: URL.init(string: TranformerDataModel.shareInstance.autoBotsData[indexPath.row].teamIcon))
            return cell
        }
        else
        {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "TranformersCells_TVC", for: indexPath)  as? TranformersCells_TVC  else {
                return UITableViewCell()
            }
            
            cell.lblName.text = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].name
            cell.lblTeamname.text = "\("TeamName:") \(TranformerDataModel.shareInstance.autoBotsData[indexPath.row].team)"
            cell.ivProfile.kf.setImage(with: URL.init(string: TranformerDataModel.shareInstance.autoBotsData[indexPath.row].teamIcon))
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == TV_AutoBots
        {
            return TranformerDataModel.shareInstance.autoBotsData.count
        }
        else
        {
            return TranformerDataModel.shareInstance.deceptionData.count
        }
        
        
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "Create_Transformer_VC") as! Create_Transformer_VC
            newViewController.fieldEdited = true
            if tableView == self.TV_AutoBots
            {
                newViewController.transformerID = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].id
                newViewController.select_Name = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].name
                newViewController.select_Strength = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].strength
                newViewController.select_Intelligence = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].intelligence
                newViewController.select_Speed = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].speed
                newViewController.select_Endurance = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].endurance
                newViewController.select_Rank = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].rank
                newViewController.select_Courage = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].courage
                newViewController.select_FirePower = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].firepower
                newViewController.select_Skill = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].skill
                newViewController.select_Team = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].team
                
            }
            else
            {
                newViewController.transformerID = TranformerDataModel.shareInstance.deceptionData [indexPath.row].id
                newViewController.select_Name = TranformerDataModel.shareInstance.deceptionData[indexPath.row].name
                newViewController.select_Strength = TranformerDataModel.shareInstance.deceptionData[indexPath.row].strength
                newViewController.select_Intelligence = TranformerDataModel.shareInstance.deceptionData[indexPath.row].intelligence
                newViewController.select_Speed = TranformerDataModel.shareInstance.deceptionData[indexPath.row].speed
                newViewController.select_Endurance = TranformerDataModel.shareInstance.deceptionData[indexPath.row].endurance
                newViewController.select_Rank = TranformerDataModel.shareInstance.deceptionData[indexPath.row].rank
                newViewController.select_Courage = TranformerDataModel.shareInstance.deceptionData[indexPath.row].courage
                newViewController.select_FirePower = TranformerDataModel.shareInstance.deceptionData[indexPath.row].firepower
                newViewController.select_Skill = TranformerDataModel.shareInstance.deceptionData[indexPath.row].skill
                newViewController.select_Team = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].team
            }
            self.navigationController?.pushViewController(newViewController, animated: true)
            
        }
        editAction.backgroundColor = .blue

        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { [self] (rowAction, indexPath) in
            
            print("Delete")
            
            if tableView == self.TV_AutoBots
            {
                TranformerDataModel.shareInstance.autoBotsData.remove(at: indexPath.row)
                let selectedRow = Int((TranformerDataModel.shareInstance.autoBotsData[indexPath.row].id as NSString).intValue)
                TranformerDataModel.shareInstance.deleteCell(id: selectedRow, complete: {
                    response in
                    self.TV_AutoBots.reloadData()
                })
            }
            else
            {
                TranformerDataModel.shareInstance.deceptionData.remove(at: indexPath.row)
                let selectedRow = Int((TranformerDataModel.shareInstance.deceptionData[indexPath.row].id as NSString).intValue)
                TranformerDataModel.shareInstance.deleteCell(id: selectedRow, complete: {
                    response in
                    self.TV_Deception.reloadData()
                })
            }
            TV_Deception.reloadData()
            TV_AutoBots.reloadData()
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        deleteAction.backgroundColor = .red

        return [editAction,deleteAction]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == TV_AutoBots {
            self.selectedAutobotsID = TranformerDataModel.shareInstance.autoBotsData[indexPath.row].id
        }
        else
        {
            self.selectedDecpticonID = TranformerDataModel.shareInstance.deceptionData[indexPath.row].id
        }
    }
}

