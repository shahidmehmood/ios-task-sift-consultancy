//
//  Create_Transformer_VC.swift
//  IOS Task 2
//
//  Created by Macbook on 4/4/21.
//

import UIKit
import DropDown
import Alamofire
class Create_Transformer_VC: UIViewController {
    
    
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var btnStrength:UIButton!
    @IBOutlet weak var btnIntelligence:UIButton!
    @IBOutlet weak var btnSpeed:UIButton!
    @IBOutlet weak var btnEndurance:UIButton!
    @IBOutlet weak var btnRank:UIButton!
    @IBOutlet weak var btnCourage:UIButton!
    @IBOutlet weak var btnFirepower:UIButton!
    @IBOutlet weak var btnSkill:UIButton!
    @IBOutlet weak var btnTeam:UIButton!
    @IBOutlet weak var btnEdit:UIBarButtonItem!
    
  var fieldEdited:Bool = Bool()
  var select_Name = ""
  var select_Strength = 0
  var select_Intelligence = 0
  var select_Speed = 0
  var select_Endurance = 0
  var select_Rank = 0
  var select_Courage = 0
  var select_FirePower = 0
  var select_Skill = 0
  var select_Team = String()
  var transformerID = String()
    
    
    
    
    let choose_Strength = DropDown()
    let choose_Intelligence = DropDown()
    let choose_Speed        = DropDown()
    let choose_Endurance    = DropDown()
    let choose_Rank         = DropDown()
    let choose_Courage      = DropDown()
    let choose_FirePower    = DropDown()
    let choose_Skill        = DropDown()
    let choose_Team         = DropDown()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDropDowns()
    }

    lazy var dropDowns: [DropDown] = {
        return [
            self.choose_Strength,
            self.choose_Intelligence,
            self.choose_Speed       ,
            self.choose_Endurance   ,
            self.choose_Rank        ,
            self.choose_Courage     ,
            self.choose_FirePower   ,
            self.choose_Skill       ,
            self.choose_Team        ,
        ]
    }()
    
    @IBAction func Action_Strength(_ sender:UIButton)
    {
        choose_Strength.show()
    }
    @IBAction func Action_Intelligence(_ sender:UIButton)
    {
        choose_Intelligence.show()
    }
    @IBAction func Action_Speed(_ sender:UIButton)
    {
        choose_Speed.show()
    }
    @IBAction func Action_Endurance(_ sender:UIButton)
    {
        choose_Endurance.show()
    }
    @IBAction func Action_Rank(_ sender:UIButton)
    {
        choose_Rank.show()
    }
    @IBAction func Action_Courage(_ sender:UIButton)
    {
        choose_Courage.show()
    }
    @IBAction func Action_FirePower(_ sender:UIButton)
    {
        choose_FirePower.show()
    }
    @IBAction func Action_Skill(_ sender:UIButton)
    {
        choose_Skill.show()
    }
    @IBAction func Action_Team(_ sender:UIButton)
    {
        choose_Team.show()
    }
    
    @IBAction func Action_CreateTeam(_ sender:UIButton)
    {
        if fieldEdited == true
        {
            guard let name = txtName.text else { return print("no name") }
            guard let teamName = btnTeam.titleLabel?.text else { return print("no name") }
            let params:Parameters = ["transformer":transformerID,"name":name,"strength":select_Strength,"intelligence":select_Intelligence,"speed":select_Speed,"endurance":select_Endurance,"rank":select_Rank,"courage":select_Courage,"firepower":select_FirePower,"skill":select_Skill,"team":teamName]
            ApiCall.Api_Call(method : HTTPMethod.put,params: params, url: BaseURl + create_Transformer_Url, complete: {
                response in
                let alert = UIAlertController(title: "Success", message: "Transformer Created Succefully", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            })
        }
        else
        {
            guard let name = txtName.text else { return print("no name") }
            guard let teamName = btnTeam.titleLabel?.text else { return print("no name") }
            let params:Parameters = ["name":name,"strength":select_Strength,"intelligence":select_Intelligence,"speed":select_Speed,"endurance":select_Endurance,"rank":select_Rank,"courage":select_Courage,"firepower":select_FirePower,"skill":select_Skill,"team":teamName]
            ApiCall.Api_Call(method : HTTPMethod.post,params: params, url: BaseURl + create_Transformer_Url, complete: {
                response in
                let alert = UIAlertController(title: "Success", message: "Transformer Created Succefully", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            })
        }
        
    }
    
    
    
    func setupDropDowns() {
        
        if fieldEdited == true
        {
            txtName.text = select_Name
            
            btnStrength.setTitle("\(select_Strength)", for: .normal)
            btnIntelligence.setTitle("\(select_Intelligence)", for: .normal)
            btnSpeed.setTitle("\(select_Speed)", for: .normal)
            btnEndurance.setTitle("\(select_Endurance)", for: .normal)
            btnRank.setTitle("\(select_Rank)", for: .normal)
            btnCourage.setTitle("\(select_Courage)", for: .normal)
            btnFirepower.setTitle("\(select_FirePower)", for: .normal)
            btnSkill.setTitle("\(select_Skill)", for: .normal)
            btnTeam.setTitle("\(select_Team)", for: .normal)
            btnEdit.title = "Edit"
        }
        else
        {
            
        }
        setupStrength()
        setup_Intelligence()
        setup_Speed()
        setup_Endurance()
        setup_Rank()
        setup_Courage()
        setup_FirePower()
        setup_Skill()
        setup_Team()
    }
}










//*****************************************

//MARK:- Create DropDowns

//*****************************************



extension Create_Transformer_VC
{
    func setupStrength() {
        var arrayMonths = [String]()
        for month in 01...10{
            arrayMonths.append("\(month)")
        }
        choose_Strength.anchorView = btnStrength
        choose_Strength.bottomOffset = CGPoint(x: 0, y: btnStrength.bounds.height)
        choose_Strength.dataSource = arrayMonths
        // Action triggered on selection
        choose_Strength.selectionAction = { [weak self] (index, item) in
            self?.btnStrength.setTitle("  " + item, for: .normal)
            self?.select_Strength = Int((item as NSString).intValue)
        }
    }
    func setup_Intelligence() {
        var arrayMonths = [String]()
        for month in 01...10{
            arrayMonths.append("\(month)")
        }
        choose_Intelligence.anchorView = btnIntelligence
        choose_Intelligence.bottomOffset = CGPoint(x: 0, y: btnIntelligence.bounds.height)
        choose_Intelligence.dataSource = arrayMonths
        // Action triggered on selection
        choose_Intelligence.selectionAction = { [weak self] (index, item) in
            self?.btnIntelligence.setTitle("  " + item, for: .normal)
            self?.select_Intelligence = Int((item as NSString).intValue)
            
        }
    }
    func setup_Speed() {
        var arrayMonths = [String]()
        for month in 01...10{
            arrayMonths.append("\(month)")
        }
        choose_Speed.anchorView = btnSpeed
        choose_Speed.bottomOffset = CGPoint(x: 0, y: btnSpeed.bounds.height)
        choose_Speed.dataSource = arrayMonths
        // Action triggered on selection
        choose_Speed.selectionAction = { [weak self] (index, item) in
            self?.btnSpeed.setTitle("  " + item, for: .normal)
            self?.select_Speed = Int((item as NSString).intValue)
        }
    }
    func setup_Endurance() {
        var arrayMonths = [String]()
        for month in 01...10{
            arrayMonths.append("\(month)")
        }
        choose_Endurance.anchorView = btnEndurance
        choose_Endurance.bottomOffset = CGPoint(x: 0, y: btnEndurance.bounds.height)
        choose_Endurance.dataSource = arrayMonths
        // Action triggered on selection
        choose_Endurance.selectionAction = { [weak self] (index, item) in
            self?.btnEndurance.setTitle("  " + item, for: .normal)
            self?.select_Endurance = Int((item as NSString).intValue)
        }
    }
    func setup_Rank() {
        var arrayMonths = [String]()
        for month in 01...10{
            arrayMonths.append("\(month)")
        }
        choose_Rank.anchorView = btnRank
        choose_Rank.bottomOffset = CGPoint(x: 0, y: btnRank.bounds.height)
        choose_Rank.dataSource = arrayMonths
        // Action triggered on selection
        choose_Rank.selectionAction = { [weak self] (index, item) in
            self?.btnRank.setTitle("  " + item, for: .normal)
            self?.select_Rank = Int((item as NSString).intValue)
        }
    }
    func setup_Courage() {
        var arrayMonths = [String]()
        for month in 01...10{
            arrayMonths.append("\(month)")
        }
        choose_Courage.anchorView = btnCourage
        choose_Courage.bottomOffset = CGPoint(x: 0, y: btnCourage.bounds.height)
        choose_Courage.dataSource = arrayMonths
        // Action triggered on selection
        choose_Courage.selectionAction = { [weak self] (index, item) in
            self?.btnCourage.setTitle("  " + item, for: .normal)
            self?.select_Courage = Int((item as NSString).intValue)
        }
    }
    func setup_FirePower() {
        var arrayMonths = [String]()
        for month in 01...10{
            arrayMonths.append("\(month)")
        }
        choose_FirePower.anchorView = btnFirepower
        choose_FirePower.bottomOffset = CGPoint(x: 0, y: btnFirepower.bounds.height)
        choose_FirePower.dataSource = arrayMonths
        // Action triggered on selection
        choose_FirePower.selectionAction = { [weak self] (index, item) in
            self?.btnFirepower.setTitle("  " + item, for: .normal)
            self?.select_FirePower = Int((item as NSString).intValue)
        }
    }
    func setup_Skill() {
        var arrayMonths = [String]()
        for month in 01...10{
            arrayMonths.append("\(month)")
        }
        choose_Skill.anchorView = btnSkill
        choose_Skill.bottomOffset = CGPoint(x: 0, y: btnSkill.bounds.height)
        choose_Skill.dataSource = arrayMonths
        // Action triggered on selection
        choose_Skill.selectionAction = { [weak self] (index, item) in
            self?.btnSkill.setTitle("  " + item, for: .normal)
            self?.select_Skill = Int((item as NSString).intValue)
        }
    }
    func setup_Team() {
        let arrayMonths = ["A","D"]
        choose_Team.anchorView = btnTeam
        choose_Team.bottomOffset = CGPoint(x: 0, y: btnTeam.bounds.height)
        choose_Team.dataSource = arrayMonths
        // Action triggered on selection
        choose_Team.selectionAction = { [weak self] (index, item) in
            self?.btnTeam.setTitle("  " + item, for: .normal)
        }
    }
}

