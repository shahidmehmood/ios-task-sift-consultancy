//
//  TranformerDataModels.swift
//  IOS Task 2
//
//  Created by Macbook on 4/4/21.
//

import Foundation
import SwiftyJSON
import Alamofire

class TranformerDataModel: NSObject {
    
    static let shareInstance:TranformerDataModel = TranformerDataModel()
    
    var autoBotsData = [TranformerModel]()
    var deceptionData = [TranformerModel]()
    
    func getDataFromApi(complete:@escaping ((String)->Void)) {
        
        ApiCall.CallingTokenApi(Parameters_Api: [:], ApiMethod: HTTPMethod.get, endURL: BaseURl + Token_URl, complete: { responseData in
            print(responseData.stringValue)
            bearer_Token = responseData.stringValue
            ApiCall.Api_Call(method : HTTPMethod.get, params: [:] ,url: BaseURl + Get_Transformer, complete: {
                response in
                let response_Data = response["transformers"].arrayValue
                for i in response_Data
                {
                    if i["team"] == "A"
                    {
                        let jsonData = TranformerModel(json: JSON(i))
                        self.autoBotsData.append(jsonData)
                    }
                    else
                    {
                        let jsonData = TranformerModel(json: JSON(i))
                        self.deceptionData.append(jsonData)
                    }
                }
                complete("Success")
            })
        })
    }
    func deleteCell(id:Int,complete:@escaping ((String)->Void))
    {
        let params:Parameters = ["transformerId":id]
        ApiCall.Api_Call(method : HTTPMethod.delete,params: params, url: BaseURl + Get_Transformer + "/" + "\(id)", complete: {
            response in
            complete("Success")
        })
    }
}
