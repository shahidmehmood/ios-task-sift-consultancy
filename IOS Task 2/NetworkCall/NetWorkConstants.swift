//
//  NetWorkConstants.swift
//  IOS Task 2
//
//  Created by Macbook on 4/2/21.
//

import Foundation
import Alamofire

//MARK:- Internet Check




class Connectivity
{
    class var isConnectedToInternet:Bool
    {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}

//MARK: - Status code error
var bearer_Token = ""

var api_Headers: HTTPHeaders = [
    "Authorization": "Bearer \(bearer_Token)"
    , "Content-Type" : "application/json"
]

//MARK:- URLS

let BaseURl = "https://transformers-api.firebaseapp.com"

let Token_URl = "/allspark"
let Get_Transformer = "/transformers"
let create_Transformer_Url = "/transformers"
