//
//  NetworkCalling.swift
//  IOS Task 2
//
//  Created by Macbook on 4/2/21.
//

import Foundation
import Alamofire
import SwiftyJSON
import SVProgressHUD

class ApiCall: NSObject {
    class func CallingTokenApi(Parameters_Api : Parameters,ApiMethod:HTTPMethod,endURL : String ,complete:@escaping ((JSON)->Void))
    {
        let url_Domain : URL = URL(string: endURL)!
        var request = URLRequest(url: url_Domain)
        request.httpMethod = ApiMethod.rawValue
        AF.request(request).responseString
        {response in
            switch response.result
            {
            case .success(let value):
                let json = JSON(value)
                complete(json)
            case .failure(let error):
                print(error)
                return
            }
        }
    }
    class func Api_Call(method:HTTPMethod,params: Parameters,url:String,complete:@escaping ((JSON)->Void))
    {
        let urls = URL.init(string: url)
        AF.request(urls!,method: method, parameters: params,headers: api_Headers).responseJSON
        {response in
            switch response.result
            {
            case .success(let value):
                let json = JSON(value)
                complete(json)
            case .failure(let error):
                print(error)
                return
            }
        }
    }
}


